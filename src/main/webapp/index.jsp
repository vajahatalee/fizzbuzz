<!doctype html>
<html>
<body>
    <h1>Welcome to the Service-Flow FizzBuzz RESTful Web Application- Vajahat Ali Niazi</h2>
    <div style="font-size:12 px;">Please click any of the following links to test the FizzBuzz REST API response. </div>
    <div style="font-size:12 px;">FizzBuzz REST API has one available service that takes a number as input and returns the response based on FizzBuzz game.</div>
    <div style="font-size:12 px; color:green">FizzBuzz URL :  /webapi/fizzbuzz/{input-number}</div>    
    <div style="font-size:12 px;">The response against REST service is returned in the form of plain text.</div>
   
    
    <p><a href="webapi/fizzbuzz/1">Test REST API with number 1</a></p>
    <p><a href="webapi/fizzbuzz/2">Test REST API with number 2</a></p>
    <p><a href="webapi/fizzbuzz/3">Test REST API with number 3</a></p>
    <p><a href="webapi/fizzbuzz/5">Test REST API with number 5</a></p>
    <p><a href="webapi/fizzbuzz/15">Test REST API with number 15</a></p>    
    <p><a href="webapi/fizzbuzz/-3">Test REST API with number -3</a></p>
	<p><a href="webapi/fizzbuzz/-5">Test REST API with number -5</a></p>
	<p><a href="webapi/fizzbuzz/-15">Test REST API with number -15</a></p>
	<p><a href="webapi/fizzbuzz/A">Test REST API with 'A'</a></p>
</body>
</html>
