package com.serviceflow.fizzbuzz;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * The FizzBuzzService implements a REST webservice for 'FizzBuzz' game Root
 * resource (exposed at "fizzbuzz" path)
 * 
 * @author Vajahat Ali Niazi
 * @version 1.0
 * @since 05.09.2016
 */
@Path("fizzbuzz")
public class FizzBuzzService {

	/**
	 * Following static constants represent Fizz and Buzz numbers that will be
	 * used to calculate the outcome. Easily configurable here.
	 */
	static final int FIZZ_NUM = 3;
	static final int BUZZ_NUM = 5;

	/**
	 * Method handling HTTP GET requests. The returned object will be sent to
	 * the client as "text/plain" media type.
	 * 
	 * @param input
	 *            should be integer and shouldn't contain decimal part or
	 *            non-integer.
	 * @return String that will be returned as a text/plain response.
	 */
	@GET
	@Path("/{param}")
	@Produces(MediaType.TEXT_PLAIN)
	public String FizzBuzz(@PathParam("param") String input) {
		int num;
		try {
			num = Integer.parseInt(input);
		} catch (NumberFormatException nfe) {
			return "Invalid input, please provide valid number";
		}
		if (num % (FIZZ_NUM * BUZZ_NUM) == 0) {
			return "FizzBuzz";
		} else if (num % BUZZ_NUM == 0) {
			return "Buzz";
		} else if (num % FIZZ_NUM == 0) {
			return "Fizz";
		} else {
			return input;
		}
	}
}
